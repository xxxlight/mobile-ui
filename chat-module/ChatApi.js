import request, { Method } from '../utils/request'
import { api } from '../config/config'

export default class ChatApi {
  constructor(client_type) {
    this.client_type = client_type || 'buyer'
  }

  api = api
  client_type = 'buyer'

  /**
   * 获取朋友列表
   * @returns {*}
   */
  getFriends() {
    return request.ajax({
      url: api.im + `/${this.client_type}/im/friends-list`,
      method: Method.GET,
      loading: false,
      needToken: true
    })
  }

  /**
   * 创建会话
   * @param receiver_id
   */
  createSession(receiver_id) {
    return request.ajax({
      url: api.im + `/${this.client_type}/im/session/${receiver_id}`,
      method: Method.POST,
      loading: false,
      needToken: true
    })
  }

  /**
   * 获取聊天历史
   * @param params
   * @returns {*}
   */
  getChatHistories(params) {
    const _params = JSON.parse(JSON.stringify(params))
    delete _params.no_data
    delete _params.loading
    return request.ajax({
      url: api.im + `/${this.client_type}/im/history`,
      method: Method.GET,
      loading: false,
      needToken: true,
      params: _params
    })
  }

  /**
   * 获取最新消息
   * @param receiver_id
   * @returns {*}
   */
  getNewMessage(receiver_id) {
    return request.ajax({
      url: api.im + `/${this.client_type}/im/history`,
      method: Method.GET,
      loading: false,
      needToken: true,
      params: { receiver_id }
    })
  }

  /**
   * 发送消息
   * @param type
   * @param receiverId
   * @param content
   * @returns {*}
   */
  sendMessage(type, receiverId, content) {
    const data = {
      content,
      receiver_id: receiverId
    }
    return request.ajax({
      url: api.im + `/${this.client_type}/im/message/${type}`,
      method: 'post',
      loading: false,
      needToken: true,
      data
    })
  }

  /**
   * 获取订单列表
   * @param params
   * @param receiverId
   */
  getOrderList(params, receiverId) {
    return request.ajax({
      url: `${api.buyer}/trade/orders/im/${receiverId}`,
      method: 'get',
      loading: false,
      needToken: true,
      params
    })
  }

  /**
   * 获取商品列表
   * @param params
   * @returns {*}
   */
  getGoodsList(params) {
    return request.ajax({
      url: `${api.buyer}/members/history/list-page`,
      method: 'get',
      loading: false,
      needToken: true,
      params
    })
  }

  /**
   * 移除未读消息数量
   * @param receiver_id
   * @returns {*}
   */
  removeUnreadNum(receiver_id) {
    return request.ajax({
      url: api.im + `/${this.client_type}/im/unread-num`,
      method: 'delete',
      loading: false,
      needToken: true,
      params: {
        sender_id: receiver_id
      }
    })
  }

  /**
   * 获取websocket连接Token
   * @returns {*}
   */
  getWebsocketToken() {
      return request.ajax({
          url: api.im + `/${this.client_type}/im/token`,
          method: 'get',
          loading: false,
          needToken: true
      })
  }
}
