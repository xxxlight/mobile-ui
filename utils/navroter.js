// 路由判断，小程序路由超过10层限制
// 避免小程序商品重复切换导致页面到结算单页时无法跳转到收货地址页，所以这里层级限制最大数为8
const MAX_VALUE = 8
export function navigateTo(url) {
    let pages = getCurrentPages()
    let dlt = ''
    // 查找目标页在页面栈的位置
    for (var i = 0; i < MAX_VALUE; i++) {
        if (pages[i]) {
            if (pages[i].$page && pages[i].$page.fullPath && (pages[i].$page.fullPath == url)) {
                dlt = i + 1; //目标页在栈中的位置
                break;
            }

        }
    }
    console.log(pages, dlt)
    // 如果有重复则返回到页面栈页面
    if (dlt) {
        uni.navigateBack({
            delta: pages.length - dlt
        })
    } else {
        if (pages.length < MAX_VALUE) {
            uni.navigateTo({ url: url })
        } else {
            uni.redirectTo({ url: url })
        }
    }
}