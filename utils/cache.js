//缓存的Key
export const Keys = {
    uid: 'uid',
    uuid: 'uuid',
    wxopenid: 'wxopenid',
    user: 'user',
    wxauth: 'wxauth',
    loginResult: 'loginResult',
    accessToken: 'accessToken',
    searchHistory: 'searchHistory',
    refreshToken: 'refreshToken',
    site: 'site',
    h5LoginLastPage: 'h5LoginLastPage',
    oldUuid: 'oldUuid'
};


//App缓存数据
class Cache {

    setItem(key, value) {
        try {
            uni.setStorageSync(key, value);
        } catch (e) {
            console.log(e);
        }
    }

    getItem(key) {
        return uni.getStorageSync(key)
    }

    removeItem(key) {
        return uni.removeStorageSync(key)
    }

    /**
     * 登录后的会员信息
     * @return {如果返回null，则需要重新登录}
     */
    user(){
       const user = uni.getStorageSync(Keys.user);
       if(user === '' || user === null){
           return null;
       }
       return user;
    }
};


export default new Cache();
