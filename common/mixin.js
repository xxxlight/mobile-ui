/** mixin */
export default {
  methods: {
    /** 获取区块链接 */
    blockHref(block) {
      if (block.block_type === 'GOODS') {
        if (!block.block_value) return
        uni.navigateTo({
            url: '/goods-module/goods?goods_id=' + block.block_value.goods_id
        })
        return
      }
      if (!block || !block.block_opt) return
      const { opt_type, opt_value } = block.block_opt
      switch (opt_type) {
        // 链接地址
        case 'URL': uni.navigateTo({ url: opt_value })
            break
        // 商品
        case 'GOODS': uni.navigateTo({ url: `/goods-module/goods?goods_id=${opt_value}` })
            break
        // 关键字
        case 'KEYWORD': uni.navigateTo({ url: `/goods-module/goods-list?keyword=${opt_value}` })
            break
        // 店铺
        case 'SHOP': uni.navigateTo({ url: `/pages/shop/shop?shop_id=${opt_value}` })
            break
        // 分类
        case 'CATEGORY': uni.navigateTo({ url: `/goods-module/goods-list?category=${opt_value}` })
            break
      }
    }
  }
}
